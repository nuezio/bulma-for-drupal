(function (Drupal) {
  Drupal.behaviors.bulmaUpdateVars = {
    attach: function (context, settings) {
      const updater = new BulmaColorUpdater(bulmaCssVarsDef);
      for(let group in settings.bulmaVars){
        for(let key in settings.bulmaVars[group]){
          updater.updateVarsInDocument(key, settings.bulmaVars[group][key]);
        }
      }
    }
  };
})(Drupal);
