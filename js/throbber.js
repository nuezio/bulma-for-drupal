/**
 * @file
 */

(function ($, Drupal) {
  Drupal.theme.ajaxProgressIndicatorFullscreen = function () {
    return '<div class="ajax-progress bulma-ajax-fullscreen">' +
      ' <div class="loader-wrapper">\n' +
      ' <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"> <circle class="path" fill="none" stroke-width="2" stroke-linecap="round" cx="33" cy="33" r="30"></circle> </svg>'
    '  </div>'
    '</div>';
  };
  Drupal.behaviors.bulmaButtonThrobber = {
    attach: function (context, settings) {

      var setProgressIndicatorThrobber = Drupal.Ajax.prototype.setProgressIndicatorThrobber;
      Drupal.Ajax.prototype.setProgressIndicatorThrobber = function () {
        if ($(this.element).hasClass('button')) {
          $(this.element).addClass('is-loading');
          this.progress.element = '';
        }
        else if ($(this.element).hasClass('file-upload-button')) {
          $(this.element).parents('.form-managed-file').addClass('button').addClass('is-loading');
        }
        else {
          setProgressIndicatorThrobber.call(this);
        }
      };
      var success = Drupal.Ajax.prototype.success;
      Drupal.Ajax.prototype.success = function (response, status) {
        $(this.element).removeClass('is-loading');
        success.call(this, response, status);
      };

    }
  };

})(jQuery, Drupal);
