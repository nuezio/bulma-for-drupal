const appColors = {
  primary: '#52B9C8',
  info: '#DB35A3',
  link: '#DB35A3',
  light: '#f5f5f5',
  dark: '#363636'
}

module.exports = {
  jsOutputFile: 'js/bulma-generated/bulma-colors.js',
  sassOutputFile: 'scss/bulma-generated/generated-bulma-vars.sass',
  cssFallbackOutputFile: 'scss/bulma-generated/generated-fallback.css',
  colorDefs: appColors,
  sassEntryFile: 'scss/main.scss',
  globalWebVar: true,
  transition: '0.5s ease'
}

